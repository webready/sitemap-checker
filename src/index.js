const {toASCII} = require('punycode');
const {format} = require('util');
const {writeFile} = require('fs-promise');
const {Spinner} = require('cli-spinner');
const moment = require('moment');
const locations = require('./locations');

require('colors');

const spinner = new Spinner('processing... %s');
spinner.setSpinnerString('⠋⠙⠹⠸⠼⠴⠦⠧⠇⠏');
spinner.start();

const domainList = require('../config/storeList').map(toASCII);
const progressStatus = {total: domainList.length, current: 0};

locations.mapLocations(domainList, saveProgress)
  .then((failedRequests) => failedRequests.filter((store) => store.failedRequests.length > 0))
  .then((filteredFailedRequests) => {
    spinner.stop(true);
    if (filteredFailedRequests.length) {
      filteredFailedRequests.map((store) => {
        console.log(store.domain.bold);
        store.failedRequests.map((failed) => {
          console.log(formatFailedLine(`${failed.url} failed with message ${failed.message}`.red));
        });
      });
    } else {
      console.log('Nothing to report!'.green);
    }
    console.log('Done!'.green);
  });

function saveProgress(domain, result) {
  spinner.stop(true);
  progressStatus.current++;
  console.log(`${progressStatus.current}/${progressStatus.total} done`);
  spinner.start();

  let resultText = 'No failed requests.';
  // Save result to logfile
  if (result.failedRequests.length) {
    resultText = result.failedRequests.reduce((text, failed) => {
      text += `${failed.url} failed with message ${failed.message}\n`;
      return text;
    }, '');
  }

  return writeFile(`output/${moment().format('YYYY-MM-DD_HH:mm:ss')}_${domain}.txt`, resultText);
}

/**
 * Indent message string
 *
 * @param str
 */
function formatFailedLine(str) {
  return format('    %s', str);
}