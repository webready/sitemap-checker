const {head} = require('got');
const sitemapParse = require('./sitemapParse');
const pMap = require('p-map');

/**
 * Gets all locations from sitemap.xml for all domains
 * given. Returns list of failed requests and its http code.
 *
 * @param domains
 * @param afterEachCb(domain, result)
 */
module.exports.mapLocations = function(domains, afterEachCb) {
  return pMap(domains, (domain) => {
    return module.exports.checkLocations(domain)
      .then((result) => {
        if (afterEachCb) {
          return afterEachCb(domain, result).then(() => result);
        }
        return result;
      });
  }, {concurrency: 1});
};

/**
 * Gets all locations from sitemap.xml for single domains
 * given. Returns list of failed requests and its http code.
 *
 * @param domain
 */
module.exports.checkLocations = function(domain) {
  return sitemapParse.getLocations(`https://${domain}/sitemap.xml`)
    .then(locations => pMap(locations, getHead, {concurrency: 1}))
    .then(result => result.filter(headCode => headCode.status !== 200))
    .then(failedRequests => {
      return {domain, failedRequests};
    })
    .catch((err) => {
      console.log(err);
      return {domain, failedRequests: {message: 'sitemap.xml broken?'}};
    });
};

const getHead = url => head(url)
  .then(res => {
    return {url, status: res.statusCode}
  })
  .catch(err => {
    return {url, status: err.statusCode, message: err.message}
  });