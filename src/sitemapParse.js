const {get} = require('got');
const {parseString} = require('xml2js');

/**
 * Parses given url for sitemap.xml and fetches urls from all <loc>-elements.
 * Ignores images and other assets.
 *
 * @param sitemapUrl
 * @returns {Promise}
 */
exports.getLocations = function(sitemapUrl) {
  return new Promise((resolve, reject) => {
    get(sitemapUrl)
      .then((res) => {
        parseString(res.body, (err, result) => {
          if (err) {
            return reject(err);
          }

          const locations = result.urlset.url.map(({loc}) => {
            return loc[0];
          });

          return resolve(locations);
        });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};
