# Webready Sitemap Checker
> Traverses locations in sitemap.xml and reports any failed requests

## Install
Run `npm install`.

## Run
Run `npm start`.